# ASP4NFA

This is an Answer Set Programming (ASP) solver-based project for inducing Non-deterministc Finite Automata (NFA).

### Requirements

1. pipetools library v. 0.3.5 or newer
2. future library v. 0.18.2 (required by FAdo port for Python 3)
3. FAdo port for Python 3 v. 1.0 available at [Fado Python 3 GitHub repository](https://github.com/0xnurl/fado-python3)
4. clingo solver v. 5.4.0 being a part of clasp ASP solver available at [Clingo GitHub repository](https://github.com/potassco/clingo/releases)

### Installation

Clone the current repository using `git clone https://gitlab.com/wojtek3dan/asp4nfa.git`.

To install pipetools and future libraries use `pip install <library name>` or `pip install -r requirements.txt`, where the `requirements.txt` file contains the following lines:

pipetools==0.3.5  
future==0.18.2

To install FAdo follow the installation instruction given at [Fado Python 3 GitHub repository](https://github.com/0xnurl/fado-python3).

To install clingo either use Anaconda or Miniconda as suggested at [Clingo GitHub repository](https://github.com/potassco/clingo/releases) or use one of the tar.gz archives available in the **Assets** section. Depending on the location to which the tar.gz archive was extracted, it may be required to change the relative path to `clingo` solver used in `nfa.py` file (lines 87 and 89). 

### Usage

```
>python nfa.py --help
usage: nfa.py [-h] [--file FILE] [--threads [THREADS]]

optional arguments:
  -h, --help           show this help message and exit
  --file FILE          an input file in Abbadingo format
  --threads [THREADS]  use 16 threads
```

Suppose that we have input.txt with examples `{a, ab, aab}` and counter-examples `{'', b, aa}`:

```
6 2
1 1 a
1 2 a b
1 3 a a b
0 0
0 1 b
0 2 a a
```

Then the result is:

```
>python nfa.py --file ./input.txt
Working with file: input.txt
Using threads: False
2
3
An automaton with 3 states:
{0}
{1}
{0: {'b': {0, 2}, 'a': {1}}, 1: {'b': {1}, 'a': {2}}, 2: {'b': {1}, 'a': {0, 1, 2}}}
Errors for positives:

Errors for negatives:

Total time = 0.04 s
```

where the two sets and a map represent, respectively, an initial state, final states, and transitions.
